'use strict';

//'ui.bootstrap', ,  'imgEditor.directives' ngRoute

var app = angular.module("getbookmarks", ['ngCookies', 'ui.router', 'services', 'angularFileUpload',
    'xeditable'])

    .config(['$stateProvider', '$urlRouterProvider', '$locationProvider', '$httpProvider',
        function ($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {

            var access = routingConfig.accessLevels;

            // Public routes
            $stateProvider
                .state('public', {
                    abstract: true,
                    template: "<ui-view/>",
                    data: {
                        access: access.public
                    }
                })
                .state('public.404', {
                    url: '/404/',
                    templateUrl: 'views/404.html'
                })
                .state('public.home', {
                    url: '/home/',
                    templateUrl: 'views/home.html'
                });

            // Anonymous routes
            $stateProvider
                .state('anon', {
                    abstract: true,
                    template: '<ui-view/>',
                    data: {
                        access: access.anon
                    }
                })
                .state('anon.login', {
                    url: '/login',
                    templateUrl: 'views/login.html'
//                controller: 'LoginCtrl'
                })
                .state('anon.register', {
                    url: '/register',
                    templateUrl: 'register',
                    controller: 'RegisterCtrl'
                });

            // Regular user routes
            $stateProvider
                .state('user', {
                    abstract: true,
                    template: "<ui-view/>",
                    data: {
                        access: access.user
                    }
                })
                .state('user.home', {
                    url: '/stories',
                    templateUrl: 'views/stories/list.html'

                })
                .state('user.new', {
                    url: '/stories/new',
                    templateUrl: 'views/stories/create.html'
                })
                .state('user.list', {
                    url: '/stories/{storyId}',
                    templateUrl: 'views/stories/create.html'
                })
                .state('user.private', {
                    abstract: true,
                    url: '/private/',
                    templateUrl: 'private/layout'
                })
                .state('user.private.home', {
                    url: '',
                    templateUrl: 'private/home'
                })
                .state('user.private.nested', {
                    url: 'nested/',
                    templateUrl: 'private/nested'
                })
                .state('user.private.admin', {
                    url: 'admin/',
                    templateUrl: 'private/nestedAdmin',
                    data: {
                        access: access.admin
                    }
                });

            // Admin routes
            $stateProvider
                .state('admin', {
                    abstract: true,
                    template: "<ui-view/>",
                    data: {
                        access: access.admin
                    }
                })
                .state('admin.admin', {
                    url: '/admin/',
                    templateUrl: 'views/administration/admin.html',
                    controller: 'AdminController'
                });


            $urlRouterProvider.otherwise('/home');

            // FIX for trailing slashes. Gracefully "borrowed" from https://github.com/angular-ui/ui-router/issues/50
            $urlRouterProvider.rule(function ($injector, $location) {

                console.log("rule");

                if ($location.protocol() === 'file')
                    return;

                var path = $location.path()
                // Note: misnomer. This returns a query object, not a search string
                    , search = $location.search()
                    , params
                    ;

                // check to see if the path already ends in '/'
                if (path[path.length - 1] === '/') {
                    return;
                }

                // If there was no search string / query params, return with a `/`
                if (Object.keys(search).length === 0) {
                    return path + '/';
                }

                // Otherwise build the search string and return a `/?` prefix
                params = [];
                angular.forEach(search, function (v, k) {
                    params.push(k + '=' + v);
                });
                return path + '/?' + params.join('&');
            });

//        $locationProvider.html5Mode(true);

            $httpProvider.interceptors.push(function ($q, $location) {
                return {
                    'responseError': function (response) {
                        console.log("interceptors")
                        if (response.status === 401 || response.status === 403) {
                            toastr.error("Такой пользователь не зарегистрирован, либо пароль не верен.");
                            $location.path('/login');
                        }
                        return $q.reject(response);
                    }
                };
            });

        }])
//    config(function ($routeProvider) {
//        $routeProvider
//            .when('/login', {templateUrl:'views/login.html'/*, controller:LoginController*/})
//            .when('/administration', {templateUrl: 'views/administration/admin.html'/*, controller: AdminController*/})
//            .when('/administration/:user', {templateUrl: 'views/stories/list.html'/*, controller: StoryListController*/})
//            .when('/:user', {templateUrl: 'views/stories/list.html'/*, controller: StoryListController*/})
//            .when('/:user/blog', {templateUrl: "blog.html"/*, controller: BlogController*/})
//            .when('/:user/stories/new', {templateUrl: 'views/stories/create.html'/*, controller: StoryCreateController*/})
//            .when('/:user/stories/:storyId', {templateUrl: 'views/stories/create.html'/*, controller: StoryCreateController*/});
//
//    });


app.run(['$rootScope', '$state', '$stateParams', 'Auth', function ($rootScope, $state, $stateParams, Auth) {

//        editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default'

    console.log("app.run");
//        console.log($state);


    $rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState, fromParams) {

        console.log("toState");
        console.log(toState);

        if (!('data' in toState) || !('access' in toState.data)) {
            console.log("Access undefined for this state");
            $rootScope.error = "Access undefined for this state";
            event.preventDefault();
        }
        else if (!Auth.authorize(toState.data.access)) {
            console.log("Seems like you tried accessing a route you don't have access to...");
            console.log("Your access role " + Auth.user.role.title);
            console.log("Need " + toState.data.access.bitMask);
            $rootScope.error = "Seems like you tried accessing a route you don't have access to...";
            event.preventDefault();

            if (fromState.url === '^') {
                if (Auth.isLoggedIn()) {
                    $state.go('user.home');
                    console.log('user.home');
                } else {
                    $rootScope.error = null;
                    $state.go('anon.login');
                    console.log('anon.login');
                }
            }
        }
    });

}]);


//app.run(function (editableOptions) {
//    editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default'
//});



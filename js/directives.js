'use strict'


var app = angular.module("getbookmarks");

app.directive('ngThumb', ['$window',
    function ($window) {
        var helper = {
            support: !!($window.FileReader && $window.CanvasRenderingContext2D),
            isFile: function (item) {
                return angular.isObject(item) && item instanceof $window.File;

            },
            isImage: function (file) {
                var type = '|' + file.type.slice(file.type.lastIndexOf('/') + 1) + '|';
                return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
            }
        };
        return {
            restrict: 'A',
            template: '<canvas/>',
            link: function (scope, element, attributes) {
                if (!helper.support) return;

                var params = scope.$eval(attributes.ngThumb);

                if (!helper.isFile(params.file)) return;
                if (!helper.isImage(params.file)) return;

                var canvas = element.find('canvas');
                var reader = new FileReader();

                reader.onload = onLoadFile;
                reader.readAsDataURL(params.file);

                function onLoadFile(event) {
                    var img = scope.img = new Image();
                    img.onload = onLoadImage;
                    img.src = event.target.result;
                }

                function onLoadImage() {
                    var width = params.width || this.width / this.height * params.height;
                    var height = params.height || this.height / this.width * params.width;
                    canvas.attr({
                        width: width,
                        height: height
                    });
                    canvas[0].getContext('2d').drawImage(this, 0, 0, width, height);
                }
            }
        };
    }]);


app.directive('fittext', function ($timeout) {
    'use strict';

    return {
        scope: {
            minFontSize: '@',
            maxFontSize: '@',
            text: '=text'
        },
        restrict: 'C',
        transclude: true,
        template: '<div ng-transclude class="textContainer" ng-bind="text"></div>',
        controller: function ($scope, $element, $attrs) {
            var maxFontSize = $scope.maxFontSize || 50;
            var minFontSize = $scope.minFontSize || 8;

            // text container
            var textContainer = $element[0].querySelector('.textContainer');

            // Add styles
            angular.element(textContainer).css('word-wrap', 'break-word');

            // max dimensions for text container
            var maxHeight = $element[0].offsetHeight;
            var maxWidth = $element[0].offsetWidth;

            var textContainerHeight;
            var textContainerWidth;
            var fontSize = maxFontSize;

            var resizeText = function () {
                $timeout(function () {
                    // set new font size and determine resulting dimensions
                    textContainer.style.fontSize = fontSize + 'px';
                    textContainerHeight = textContainer.offsetHeight;
                    textContainerWidth = textContainer.offsetWidth;

                    if ((textContainerHeight > maxHeight || textContainerWidth > maxWidth) && fontSize > minFontSize) {

                        // shrink font size
                        var ratioHeight = Math.floor(textContainerHeight / maxHeight);
                        var ratioWidth = Math.floor(textContainerWidth / maxWidth);
                        var shrinkFactor = ratioHeight > ratioWidth ? ratioHeight : ratioWidth;
                        fontSize -= shrinkFactor;
                        // console.log("fontSize", fontSize);
                        resizeText();
                    } else {
                        /*textContainer.style.visibility = "visible";*/
                    }
                }, 0);
            };

            // watch for changes to text
            $scope.$watch('text', function (newText, oldText) {
                if (newText === undefined) return;
                if (newText == null) return;
                if (oldText == null) return;

                // text was deleted
                if (oldText !== undefined && newText.length < oldText.length) {
                    fontSize = maxFontSize;
                    // console.log("Letter was deleted");
                }
                /*textContainer.style.visibility = "hidden";*/
                resizeText();
            });
        }
    };
});

app.directive("contenteditable", function () {
    return {
        restrict: "A",
        require: "ngModel",
        link: function (scope, element, attrs, ngModel) {

            function read() {
                ngModel.$setViewValue(element.html());
            }

            ngModel.$render = function () {
                element.html(ngModel.$viewValue || "");
            };

            element.bind("blur keyup change", function () {
                scope.$apply(read);
            });

            element.bind('keydown', function (event) {
                console.log("keydown " + event.which);
                var enter = event.which == 13,
                    el = event.target;

                if (enter) {
                    console.log("esc");
                    ngModel.$setViewValue(element.html());
                    element.blur();
                    event.preventDefault();
                }

            });
        }
    };
});

app.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
// save on blur
//                scope.$apply(function (){
//                    scope.$eval(attrs.ngEnter);
//                });

                if (event.shiftKey === true) {
                    // nothing
                    return;

                }
                else {
                    // run your function
                    event.preventDefault();
                    element.blur();
                }

            }
        });
    };
});

app.directive('accessLevel', ['Auth', function(Auth) {
    return {
        restrict: 'A',
        link: function($scope, element, attrs) {
            var prevDisp = element.css('display')
                , userRole
                , accessLevel;

            $scope.user = Auth.user;
            $scope.$watch('user', function(user) {
                if(user.role)
                    userRole = user.role;
                updateCSS();
            }, true);

            attrs.$observe('accessLevel', function(al) {
                if(al) accessLevel = $scope.$eval(al);
                updateCSS();
            });

            function updateCSS() {
                if(userRole && accessLevel) {
                    if(!Auth.authorize(accessLevel, userRole))
                        element.css('display', 'none');
                    else
                        element.css('display', prevDisp);
                }
            }
        }
    };
}]);

app.directive('activeNav', ['$location', function($location) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var anchor = element[0];
            if(element[0].tagName.toUpperCase() != 'A')
                anchor = element.find('a')[0];
            var path = anchor.href;

            scope.location = $location;
            scope.$watch('location.absUrl()', function(newPath) {
                path = normalizeUrl(path);
                newPath = normalizeUrl(newPath);

                if(path === newPath ||
                    (attrs.activeNav === 'nestedTop' && newPath.indexOf(path) === 0)) {
                    element.addClass('active');
                } else {
                    element.removeClass('active');
                }
            });
        }

    };

    function normalizeUrl(url) {
        if(url[url.length - 1] !== '/')
            url = url + '/';
        return url;
    }

}]);

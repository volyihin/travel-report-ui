/**
 * Created by nataliyamakarova on 15.10.14.
 */

var app = angular.module('getbookmarks');

app.controller('StoryListController', ['$scope', '$rootScope','Auth','Story',
    function ($scope, $rootScope,Auth, Story) {

        var currentUser = Auth.user
//        toastr.info("Вошли как " + currentUser.username);


        $scope.stories = Story.query({userId:currentUser.id});

        $scope.delete = function (index) {
            console.log("Delete story");
            $scope.story = $scope.stories[index];
            $scope.stories.splice(index, 1);

            $scope.story.$delete(function (story, headers) {
                toastr.success("История удалена");
            });
        }

    }]);

app.controller('StoryCreateController', ['$scope', '$rootScope', '$location', 'Story', 'Record', 'FileUploader', function ($scope, $rootScope, $location, Story, Record, FileUploader) {

   var storyId = undefined;
//       $routeParams.storyId;
    $scope.isCollapsed = true;

    if (storyId == undefined) {
        $scope.story = new Story();
        $scope.story.title = "Новый заголовок";
        $scope.story.$save();

    } else {
        $scope.story = Story.get({storyId: storyId});
    }

    $scope.newRecord = new Record();


    var uploader = $scope.uploader = new FileUploader({
        url: '/api/v1/upload',
        autoUpload: true,
        removeAfterUpload: true
    });

    // FILTERSs

    uploader.filters.push({
        name: 'imageFilter',
        fn: function (item /*{File|FileLikeObject}*/, options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });

    $scope.selectStoryImage = function (record) {
        console.log("selectStoryImage");
        $scope.story.image = record.image;
        $scope.story.$save(function (response) {
            toastr.success("Выбрана обложка истории");
            console.info($scope.story.id);
        });
    }


    $scope.addRecord = function () {
        console.log("Add record");

        console.log($scope.newRecord);

        $scope.newRecord.$save({storyId: $scope.story.id}, function (record, headers) {
            toastr.success("Создана новая запись");
        }).then(function (response) {
            $scope.story.records.push(response);

        });

        $scope.newRecord = new Record();

    }

    $scope.updateStoryText = function (story, data) {
        console.log("update story");
        console.log(story)

        $scope.story.text = data;
        $scope.story.$save(function (response) {
            toastr.success("История сохранена");
            console.info($scope.story.id);
        }, function (responce) {
            toastr.error("Не удалось обновить историю");
        });
//        $scope.isCollapsed = true;

    };

    $scope.updateStory = function (story) {
        console.log("update story");
        console.log(story)

        $scope.story.$save(function (response) {
            toastr.success("История сохранена");
            console.info($scope.story.id);
        });
        $scope.isCollapsed = true;

    };

    $scope.updateRecord = function (record, data) {
        console.log("update record storyId" + $scope.story.id);
        console.log("update record recordId" + record.id);
        console.log("update record comment" + data);
        var updatedRecord = Record.get({ storyId: $scope.story.id, recordId: record.id}, function (response) {
            updatedRecord.comment = data;
            updatedRecord.$update({storyId: $scope.story.id}, function () {
                toastr.success("Обновили комментарий");
            });
        });

    };

    $scope.add = function () {
        console.log("Add item");

        console.log($scope.newRecord);

        $scope.newRecord.$save({storyId: $scope.story.id}, function (record, headers) {
            toastr.success("Создана новая запись");
        }).then(function (response) {
            $scope.story.records.push(response);

        });

        $scope.newRecord = new Record();

    }

    $scope.delete = function (index) {
        $scope.record = $scope.story.records[index];

        console.log("Delete record " + $scope.story.id + " " + $scope.record.id);
        $scope.story.records.splice(index, 1);

        var deletedRecord = new Record();

        deletedRecord.$delete({storyId: $scope.story.id, recordId: $scope.record.id}, function (record, headers) {
            toastr.success("Запись удалена");
        });
    }


    // CALLBACKS
    uploader.onBeforeUploadItem = function (item) {
        console.log(item);
        var mydata = {};
        // isStory
        if (item.isStory == true) {
            mydata.isStory = true;
            item.url = '/api/v1/upload/story'
        }
        // isRecord
        if ($scope.story.id == undefined) {
            mydata.storyId = '';
        } else {
            mydata.storyId = $scope.story.id;
        }

        if (item.text == undefined) {
            mydata.comment
        } else {
            mydata.comment = item.text;
        }

        mydata.username = $rootScope.username;
        item.formData.push(mydata);

        item.formData.push();
        console.info('onBeforeUploadItem', item);
    };
    uploader.onSuccessItem = function (fileItem, response, status, headers) {
        console.info('onSuccessItem', fileItem, response, status, headers);


        if (response.comment == undefined) {
            console.info('response is image ', response);
            $scope.story.image = response;
        } else {
            console.info('response is record ', response);
            $scope.story.records.push(response);

        }
    };


    $scope.save = function () {
        $scope.story.$save(function (story, headers) {
            toastr.success("Создана новая история");
            $location.path("/" + $rootScope.username + "/");
        });
    };

    $scope.rotate = function (angle) {
        $scope.angle = angle;
    };

}]);

app.controller('LoginCtrl',
    ['$rootScope', '$scope', '$location', '$window', 'Auth', function ($rootScope, $scope, $location, $window, Auth) {

        $scope.rememberme = true;

        $scope.login = function () {
            Auth.login({
                    username: $scope.username,
                    password: $scope.password,
                    rememberme: $scope.rememberme
                },
                function (res) {
                    $location.path('/stories');
//                    $state.go('user.home')
                },
                function (err) {
                    $rootScope.error = "Failed to login";
                });
        };

        $scope.loginOauth = function (provider) {
            $window.location.href = '/auth/' + provider;
        };
    }]);


app.controller('NavCtrl', ['$rootScope', '$scope', '$location', 'Auth', '$state',
                  function ($rootScope, $scope, $location, Auth, $state) {
    $scope.user = Auth.user;
    $scope.userRoles = Auth.userRoles;
    $scope.accessLevels = Auth.accessLevels;

    $scope.logout = function () {
        Auth.logout(function () {
            $location.path('/home');
//        $state.go('public.home');
        }, function () {
            $rootScope.error = "Failed to logout";
        });
    };
}]);

app.controller('RegisterCtrl',
    ['$rootScope', '$scope', '$location', 'Auth', function ($rootScope, $scope, $location, Auth) {
        $scope.role = Auth.userRoles.user;
        $scope.userRoles = Auth.userRoles;

        $scope.register = function () {
            Auth.register({
                    username: $scope.username,
                    password: $scope.password,
                    role: $scope.role
                },
                function () {
                    $location.path('/');
                },
                function (err) {
                    $rootScope.error = err;
                });
        };
    }]);
//
//function BlogController($scope, $routeParams, User) {
//    console.log($routeParams.user);
//    $scope.user = User.get($routeParams.user);
//    console.log($scope.user);
//    $scope.username = $routeParams.user;
//
//
//}

app.controller('AdminController', ['$scope', 'User', function ($scope, User) {
    $scope.users = User.query();

}]);




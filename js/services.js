/**
 * Created by nataliyamakarova on 15.10.14.
 */


var services = angular.module("services", ["ngResource"])

services.factory('Story', function ($resource) {
    var Story = $resource('/api/v1/stories/:storyId', {storyId: '@id'},
        {
            'update': { method: 'PUT' },
            'query': {method:'GET', params:{userId:'userId'}, isArray:true}
        });
    Story.prototype.isNew = function () {
        return (typeof(this.id) === 'undefined');
    }
    return Story;
});

services.factory('Record', function ($resource) {
    var Record = $resource('/api/v1/stories/:storyId/records/:recordId', {storyId: 'storyId', recordId: 'recordId'}, {
        'update': { method: 'PUT', params: {recordId: '@id'}}
    });
    Record.prototype.isNew = function () {
        return (typeof(this.id) === 'undefined');
    }
    return Record;
});

services.factory('Auth', function($http, $cookieStore){

    var accessLevels = routingConfig.accessLevels
        , userRoles = routingConfig.userRoles
        , currentUser = $cookieStore.get('user') || { username: '', role: userRoles.public };

//    $cookieStore.remove('user');

    function changeUser(user) {
        angular.extend(currentUser, user);
    }

    return {
        authorize: function(accessLevel, role) {
            if(role === undefined) {
                role = currentUser.role;
            }

            if (role === null) {
                console.error("Role must be set");
            }

            return accessLevel.bitMask & role.bitMask;
        },
        isLoggedIn: function(user) {
            if(user === undefined) {
                user = currentUser;
            }
            return user.role.title === userRoles.user.title || user.role.title === userRoles.admin.title;
        },
        register: function(user, success, error) {
            $http.post('/register', user).success(function(res) {
                changeUser(res);
                success();
            }).error(error);
        },
        login: function(user, success, error) {

            $http.post('/api/v1/users/login', user).success(function(user){
                console.log(user);
                changeUser(user);
                $cookieStore.put('user', user);
                success(user);
            }).error(error);
        },
        logout: function(success, error) {
            $http.post('/api/v1/users/logout',currentUser).success(function(){
                changeUser({
                    username: '',
                    role: userRoles.public
                });
                $cookieStore.remove('user');
                success();
            }).error(error);
        },
        accessLevels: accessLevels,
        userRoles: userRoles,
        user: currentUser
    };
});

services.factory('User', function ($resource) {
    var User = $resource('/api/v1/users/:username', {username: '@username'});
    User.prototype.isNew = function () {
        return (typeof(this.id) === 'undefined');
    }
    return User;
});

services.factory('Users', function($http) {
    return {
        getAll: function(success, error) {
            $http.get('/users').success(success).error(error);
        }
    };
});